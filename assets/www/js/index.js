/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var db;
var dataset;
var status;
try{
		db = window.openDatabase("ITMISSION", "1.0", "IT MISSION DB", 200000); //will create database Dummy_DB or open it
		
		// alert("DATABSE CREATED SUCCEESSFULLY");
		
}catch(e)
{
		alert("DATABASE ERROR ! APP NEED TO CLOSE");
		close_app();
}
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
function Devices()
{
	
	document.addEventListener("backbutton", onBackKeyDown, false);
	createDB();
	
}
function login()
{
	try
	{
		var uname  = $("#uname").val();
		var password   = $("#upass").val();
		
		db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM Credentials WHERE Username=? AND Password=?', [uname, password], function(tx, results) {
             if (results.rows.length > 0) {
                window.location = "userprofile.html";
             } else {
            	 alert("INVALID USER NAME & PASSWORD");
             }
        }, errorCB);
    });
		
	}catch(log_err)
	{
		alert(log_err);
	}
}
function createDB()
{
	// alert("WELC");
	try
	{
		db.transaction(function (tx) {  
			tx.executeSql('CREATE TABLE IF NOT EXISTS Credentials (id INTEGER PRIMARY KEY AUTOINCREMENT, Username TEXT NOT NULL, Password TEXT NOT NULL)');
			});
		
	}
	catch(ct)
	{
		alert("CREATE TABLE ERROR"+ct);
	}

	
}
function register(){
	 try
	 {
		 
	 
   // alert("CLicked");
						  
    var passwordcheck = $('#r_pass').val();
	 
	// alert(passwordcheck);
	 
	 var confirmpasswordcheck = $('#r_cpass').val();
	 
	// alert(confirmpasswordcheck);
	 
	 if(passwordcheck == confirmpasswordcheck){
		 
		//  alert(passwordcheck+","+confirmpasswordcheck);
		
		 run_insert();				
		
	 }
	 
	 else {
		
		alert("Password and Confirm Password Mismatch");
	 }
	 }catch(reg_error)
	 {
		 alert("hii im REG ERROR"+reg_error);
	 }

}
function queryDB(tx){
    //tx.executeSql('SELECT * FROM Credentials',[Username,Password],querySuccess,errorCB);
	try{
		
	
	/* db.transaction(function (tx) {  
		 tx.executeSql('SELECT * FROM Credentials',[Username,Password],querySuccess,errorCB);
		}); */

        db.transaction(function (tx) {
           tx.executeSql('SELECT * FROM Credentials', [], function (tx, results) {
        	   document.querySelector('#datas').innerHTML =  "";
              var len = results.rows.length, i;
             // msg1 = "<p>Found rows: " + len + "</p>";
             // document.querySelector('#datas').innerHTML +=  msg1;
					
              for (i = 0; i < len; i++){
                 msg = "<li>" + results.rows.item(i).Username + "   <br/>"+results.rows.item(i).Password+"</li>";
                 document.querySelector('#datas').innerHTML +=  msg;
              }
              $('#datas').listview();
              $('#get').addClass('ui-disabled');
           }, null);
        });
        
	}catch(exe_exp)
	{
		alert(exe_exp)
	}
}

function run_insert()
{
	try
	{
		   var username = $('#r_name').val();
			var password = $('#r_pass').val();   
			db.transaction(function (tx) {  
				tx.executeSql("INSERT INTO Credentials (Username, Password) VALUES (?,?)", [ username , password ]);
				});
	        alert("INSERTION SUCCESS");
	        window.location.href = "#otp";
	        
	}
	catch(ins_err)
	{
		alert("IM INS ERROR"+ins_err);
	}
}
function close_app()
{
	navigator.app.exitApp(); 
}

function init() {
    document.addEventListener("deviceready", Devices, true);
    
    
}   
function onBackKeyDown() {
	var r = confirm("Exit Application ?");
	
	if (r == true) {
		
		navigator.app.exitApp(); 
		
	} else {
	    x = "You pressed Cancel!";
	    
	}
	
}
function access_base()
{

cordova.exec(sayHelloSuccess, sayHelloFailure, "Customs", "rawdata", []);

}
function add()
{
var a = $("#num1").val();
var b = $("#num2").val();
var datas = [{"num1":a,"num2":b}];
cordova.exec(addsuccess, addfailure, "Customs", "add", [datas]);
}
function addsuccess(data)
{
$("#result").html(data);
}
function addfailure(data)
{
alert(data);
}
function sayHelloSuccess(data){
   alert("DATA FROM JAVA IS  "+data);
}

function sayHelloFailure(data){
  alert("ERROR DATA"+data);
}
function errorCB(error) {
   
}
function launch()
{
//var ref = window.open('http://apache.org', '_blank', 'location=yes');
cordova.exec(sayHelloSuccess, sayHelloFailure, "Customs", "open", []);
}
//function will be called when process succeed
function successCB() {

   // db.transaction(queryDB,errorCB);
}
init();
app.initialize();