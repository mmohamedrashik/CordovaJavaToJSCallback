package com.aa.ss;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2/25/2016.
 */
public class Customs extends CordovaPlugin{
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

    }
    public boolean execute(final String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(action.equals("rawdata")) {
            Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), "JAVA LAYER ACCESSED", Toast.LENGTH_SHORT);
            toast.show();

                callbackContext.success("SSSS");



        }
        else if(action.equals("add"))
        {
            JSONArray data = args.getJSONArray(0);
            JSONObject data2 = data.getJSONObject(0);
            Object data3 = data2.get("num1");
            Object data4 = data2.get("num2");
            int number1 = Integer.parseInt(data3.toString());
            int number2 = Integer.parseInt(data4.toString());
            int result = number1+number2;
           // Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), "DATA FETCHED"+result, Toast.LENGTH_SHORT);
           // toast.show();
            callbackContext.success(result);
        }
        else if(action.equals("open"))
        {
        Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(),PaymentGateway.class);
            this.cordova.startActivityForResult(this,intent,0);
        }
        else
        {

        }
        return true;

    }

}
